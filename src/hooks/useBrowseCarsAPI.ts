import * as React from 'react';
import mfindApi from '../api/mfind.api';

export interface Car {
  make_code: string;
  make_name: string;
}

export const useBrowseCarsAPI = () => {
  const [cars, setCars] = React.useState<Car[]>([]);

  React.useEffect(() => {
    const fetch = async () => {
      const response = await mfindApi.get('cars');

      setCars(response.data);
    };

    fetch();
  }, []);

  return { cars };
};
