import * as React from 'react';
import mfindApi from '../api/mfind.api';

export interface Fuel {
  fuel_code: string;
  fuel_name: string;
}

export const useBrowseFuelsAPI = (brand?: string, model?: string) => {
  const [fuels, setFuels] = React.useState<Fuel[]>([]);

  React.useEffect(() => {
    const fetch = async () => {
      const response = await mfindApi.get(
        `cars/${brand}/models/${model}/fuels/`
      );

      setFuels(response.data);
    };

    if (brand && model) fetch();
    else setFuels([]);
  }, [brand, model]);

  return { fuels };
};
