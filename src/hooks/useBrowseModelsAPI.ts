import * as React from 'react';
import mfindApi from '../api/mfind.api';

export interface Model {
  model_name: string;
}

export const useBrowseModelsAPI = (brand?: string) => {
  const [models, setModels] = React.useState<Model[]>([]);

  React.useEffect(() => {
    const fetch = async () => {
      const response = await mfindApi.get(`cars/${brand}/models`);

      setModels(response.data);
    };

    if (brand) fetch();
    else setModels([]);
  }, [brand]);

  return { models };
};
