import { Button, Link } from '@mui/material';
import * as React from 'react';
import { PunktaForm } from '../../components/PunktaForm';
import { Car, useBrowseCarsAPI } from '../../hooks/useBrowseCarsAPI';
import {
  Fuel,
  useBrowseFuelsAPI,
} from '../../hooks/useBrowseFuelsAPI';
import {
  Model,
  useBrowseModelsAPI,
} from '../../hooks/useBrowseModelsAPI';

interface Form
  extends Pick<Car, 'make_name'>,
    Pick<Model, 'model_name'>,
    Pick<Fuel, 'fuel_name'> {}

const PunktaFormHandler: React.FunctionComponent<unknown> = () => {
  const [form, setForm] = React.useState<Form>({} as Form);

  const { cars } = useBrowseCarsAPI();

  const { models } = useBrowseModelsAPI(form?.make_name);

  const { fuels } = useBrowseFuelsAPI(
    form?.make_name,
    form?.model_name
  );

  const handleChange = React.useCallback(
    (data: { name: string; value: any }) => {
      setForm(
        data.name === ('make_name' as keyof Form)
          ? ({
              [data.name]: data.value,
            } as Form)
          : {
              ...form,
              [data.name]: data.value,
            }
      );
    },
    [form]
  );

  const redirectUrl = React.useMemo(() => {
    const paramsString = Object.keys(form)
      .map((key) => key + '=' + (form as any)[key])
      .join('&');

    const url = `https://punkta.pl/ubezpieczenie-oc-ac/kalkulator-oc-ac?${paramsString}`;

    return url;
  }, [form]);

  return (
    <PunktaForm
      title="punkta"
      description="oszczedz nawet 580 zlotych na oc"
      buttonSubmitText="oblicz skladke"
      pending={!cars.length}
      onChange={handleChange}
      panels={[
        {
          name: 'make_name' as keyof Form,
          label: 'Marka',
          value: form.make_name,
          options: cars.map((car) => ({
            label: car.make_name,
            value: car.make_name,
          })),
        },
        {
          name: 'model_name' as keyof Form,
          label: 'Model',
          value: form.model_name,
          options: models.map((model) => ({
            label: model.model_name,
            value: model.model_name,
          })),
        },
        {
          name: 'fuel_name' as keyof Form,
          label: 'Typ paliwa',
          value: form.fuel_name,
          options: fuels.map((fuel) => ({
            label: fuel.fuel_name,
            value: fuel.fuel_name,
          })),
        },
      ]}
      extraContent={
        <Link
          fullWidth
          component={Button}
          href={redirectUrl}
          target="_blank"
          rel="noopener"
          style={{
            marginTop: '1.5rem',
            backgroundColor: '#ffcc00',
            color: 'black',
            fontWeight: 600,
            opacity: !Object.keys(form).length ? 0.4 : 1,
          }}
        >
          oblicz skladke
        </Link>
      }
    />
  );
};

export default PunktaFormHandler;
