import { Grid } from '@mui/material';
import React from 'react';
import PunktaFormHandler from './organism/PunktaFormHandler';

function App() {
  return (
    <div className="App">
      <Grid container alignItems="center" spacing={3}>
        <Grid item xs={12} md={2}>
          <PunktaFormHandler />
        </Grid>
        <Grid item xs={12} md={2}>
          <PunktaFormHandler />
        </Grid>
        <Grid item xs={12} md={2}>
          <PunktaFormHandler />
        </Grid>
      </Grid>
    </div>
  );
}

export default App;
