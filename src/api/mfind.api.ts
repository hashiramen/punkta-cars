import axios from 'axios';

export default axios.create({
  baseURL: 'https://api-dev.mfind.pl',
  headers: {
    Authorization: 'Basic YXV0a2FfYXBpOmF1dGthX2FwaV8yMDE5',
  },
});
