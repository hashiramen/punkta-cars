import {
  CardContent,
  CardHeader,
  CircularProgress,
  FormControl,
  InputLabel,
  MenuItem,
  Select,
} from '@mui/material';
import React, { ReactElement } from 'react';
import Card from '../Card';
import styled from 'styled-components';

const FormCardWrapper = styled(Card)`
  position: relative;
`;

const FormControlWrapper = styled(FormControl)`
  margin-top: 1.5rem;
  &:first-child {
    margin-top: 0;
  }
`;

const SpinnerWrapper = styled.div`
  height: 100%;
  width: 100%;
  position: absolute;
  top: 0;
  left: 0;
  display: flex;
  align-items: center;
  justify-content: center;
`;

const SelectWrapper = styled(Select)`
  &.MuiSelect-select {
    font-size: 5px;
  }
`;

interface FormDropdownPanel {
  name: string;
  label: string;
  disabled?: boolean;
  value?: any;
  options: {
    label: string;
    value: string | number;
  }[];
}

export interface PunktaFormProps {
  title: string;
  description: string;
  buttonSubmitText: string;
  panels: FormDropdownPanel[];
  pending?: boolean;
  onChange: (data: { name: string; value: any }) => void;
  extraContent?: React.ReactNode | JSX.Element;
}

export function PunktaForm({
  title,
  description,
  panels,
  pending,
  onChange,
  extraContent,
}: PunktaFormProps): ReactElement {
  return (
    <FormCardWrapper>
      {pending && (
        <SpinnerWrapper>
          <CircularProgress />
        </SpinnerWrapper>
      )}
      <CardHeader title={title} subheader={description} />
      <CardContent>
        {panels.map((panel, index) => (
          <FormControlWrapper
            key={panel.label}
            fullWidth
            variant="filled"
            disabled={panel.disabled || !panel.options.length}
          >
            <InputLabel id="label-2">{panel.label}</InputLabel>
            <SelectWrapper
              name={panel.name}
              labelId={`${panel.label}-label-${index}`}
              id="demo-simple-select"
              value={panel.value ?? 'null'}
              label={panel.label}
              onChange={(e) =>
                void onChange({
                  name: e.target.name,
                  value: e.target.value,
                })
              }
            >
              {!panel.options.length && (
                <MenuItem value="null"></MenuItem>
              )}
              {panel.options.map((option) => (
                <MenuItem key={option.value} value={option.value}>
                  {option.label}
                </MenuItem>
              ))}
            </SelectWrapper>
          </FormControlWrapper>
        ))}
        {extraContent}
      </CardContent>
    </FormCardWrapper>
  );
}
